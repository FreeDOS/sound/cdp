
English.Zip contains the audio CD player with English texts.

The CDP audio CD player is Freeware. That means that you can use this
program without paying any fee and that you can distribute this program
as long as you keep all the files together.
Besides of that, permission is granted that you ship CDP within your own
packages as long as you leave a note in the documentation.



Deutsch.Zip enth�lt den Audio-CD-Player mit deutschen Texten.

Der CDP Audio-CD-Player ist Freeware. Das bedeutet, da� Sie dieses
Programm ohne Bezahlung nutzen d�rfen und da� Sie dieses Programm
weitergeben d�rfen, sofern Sie keine Dateien aus dem Paket entfernen.
Dar�berhinaus erhalten Sie Erlaubnis, CDP in eines Ihrer Pakete
aufzunehmen, sofern Sie dies in der Dokumentation kenntlich machen.
