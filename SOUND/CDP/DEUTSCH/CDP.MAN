
     CDP (12 Feb 1996)       Command Line CD Player                     CDP



     NNAAMMEE: 

          CDP - Kommandozeilen Audio-CD Player 

     SSYYNNOOPPSSIISS: 

          _c_d_p [{ooppttiioonn | kkoommmmaannddoo}] 
          

     DDEESSCCRRIIPPTTIIOONN: 

          _c_d_p  ist  ein  kommandozeilenorientierter  Audio-CD Spieler,
          welcher auf die Dienste  von  MSCDEX  verzichtet  und  nicht
          resident im Speicher verbleibt (kein TSR).  
          
          MSCDEX  nicht  zu ber�cksichtigen spart circa 16KB Speicher,
          erfordert  aber  auch  etwas  mehr  Unterst�tzung  bei   der
          Installation  durch den Nutzer. Auch bedeutet das, da� jetzt
          zwar Audiospuren abgespielt werden k�nnen, jedoch nicht  auf
          die Datenspuren   zugegriffen   werden  kann.    Ist  MSCDEX
          geladen, funktioniert _c_d_p trotzdem.  
          
          _c_d_p verbleibt nach Programmende nicht im Speicher. Das spart 
          Speicher  und  Rechenzeit,  doch   konnten   Funktion   wie:
          zzuuff�lllliiggeess  AAbbssppiieelleenn  und  eennddlloosseess AAbbssppiieelleenn deshalb nicht
          implementiert werden.  
          
          Alle Optionen und Kommandos werden  von  links  nach  rechts
          abgearbeitet.  Vergleiche hierzu die Sektion EXAMPLES.  
          
          Falls  kein  Kommando  angegeben wurde, wird in Abh�ngigkeit
          vom Dateinamen ein Standardkommando ausgef�hrt.   Vergleiche
          hierzu Sektion DEFAULT ACTION.  
          
          Bevor  ein Kommandozeilenargument ausgewertet wird, wird der
          Inhalt der Umgebungsvariablen  "SSKKAAUUSS1188AA"  gelesen.    Deren
          Inhalt  wird  genauso  interpretiert,  als  w�re er Teil der
          Kommandozeile, au�er da� alle Argumente, welche keine Option 
          darstellen, ignoriert  werden.    Wurde  zum  Beispiel   die
          Umgebungsvariable durch "SET SKAUS18A= /D:NECD001 /U:2 play" 
          gesetzt,  verhalten  sich  alle weiteren Aufrufe von _c_d_p so,
          als ob "/D:NECD001" und "/U:2" als die ersten zwei Argumente 
          angegeben w�ren.    Das  Kommando  "play"  wird   ignoriert.
          Unbekannte Optionen l�sen einen Fehler aus.  
          

     OOPPTTIIOONNSS: 

          
      _/_?_ _a_n_d_ _/_h Ausgabe eines Hilfebildschirms.  
          
      _/_D_:_n_a_m_e Device  name.   Verwendung von nnaammee als Treibernamen, um
          mit dem CD-ROM Treiber zu kommunizieren.  Dies  ist  in  den
          meisten  F�llen  derselbe  Name,  wie  in  der gleichnamigen
          Option Ihres CD-ROM Treibers bzw.  von MSCDEX.  Falls  nicht
          durch   den   Aufruf   von   _C_D_P__I_N_S_T   ge�ndert,   ist  der
          Standardname: MSCD001.  


                                      -1-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          
      _/_D Setzt den Treibernamen auf den Standardnamen zur�ck.  
          
      _/_U_:_# Unit number.  Verwendung der  Einheit  #  als  Subunit  des
          CD-ROM Ger�tes.    Falls nicht durch den Aufruf von _C_D_P__I_N_S_T
          ge�ndert, ist der Standardwert: 0.  
          
      _/_U Setzt die Einheitennummer wieder auf den Standard zur�ck.  
          
      _/_[_+_-_]_A Auto-Unlock. Falls das '+' Zeichen  angegeben  ist,  wird
          Auto-Unlock aktiviert.  Falls das '-' Zeichen angegeben ist, 
          wird Auto-Unlock  deaktiviert.   Ansonsten wird der aktuelle
          Zustand umgeschalten.  Falls  nicht  durch  den  Aufruf  von
          _C_D_P__I_N_S_T ge�ndert, ist der Standardzustand: deaktiviert.  
          
          CD-ROM Ger�te mit Schlitten erlauben das automatische �ffnen 
          und  Schlie�en des Schlittens. Um Probleme zu vermeiden, die
          aus dem  �ffnen  der  T�r  zu  einem  ung�nstigen  Zeitpunkt
          resultieren,  ist  es m�glich, das �ffnen zu verhindern (der
          Schlitten wird gelockt/verriegelt).  Ist Auto-Unlock  aktiv,
          wird  dieser  Schutz  automatisch  zur�ckgesetzt,  wenn  der
          Schlitten  ge�ffnet  werden  soll,  anderenfalls  wird  eine
          Fehlermeldung ausgegeben.  
          
      _/_V Anzeige der Versionsinformation, dann Programm beenden.  
          

     CCOOMMMMAANNDDSS: 

          
          Jedes  Kommando  kann  bis  zu  den  Zeichen  links  von der
          schlie�enden Klammer   abgek�rzt   werden.      Gro�-    und
          Kleinschreibung wird ignoriert.  
          
          Falls   ein   Kommando   eine  geschlossene  T�r  zu  seiner
          Ausf�hrung erwartet, ist der Schlitten  jedoch  offen,  wird
          automatisch ein "close" Komamndo ausgef�hrt.  
          
      _e_)_j_e_c_t  �ffnet  den  Schlitten.  Ist  der  Schlitten  verriegelt
          (gelockt), und ist Auto-Unlock deaktiviert, wird ein  Fehler
          ausgel�st.   Nicht alle CD-ROM Laufwerke unterst�tzen dieses
          Kommando.  
          
      _c_)_l_o_s_e Schliessen des Schlittens und Initialisierung des  CD-ROM
          Treibers.     Diese    Prozedur    stoppt    ebenfalls   die
          Audiowidergabe. Nicht  alle  CD-ROM  Laufwerke  unterst�tzen
          dieses Kommando.  
          
      _s_)_t_a_t_u_s  Anzeige  des Status des CD-ROM Treibers. Hierbei werden
          mehr oder weniger n�tzliche Informationen ausgegeben.  
          
      _s_t_o_)_p Dieses Kommando ist identisch zu cclloossee.  
          
      _l_)_o_c_k Lock door.   Schlitten  verriegeln.    Nicht  alle  CD-ROM
          Laufwerke unterst�tzen dieses Kommando.  
          
      _u_)_n_l_o_c_k Unlock door.    Schlitten entriegeln.  Nicht alle CD-ROM
          Laufwerke unterst�tzen dieses Kommando.  


                                      -2-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          
      _t_)_r_a_c_k_s Anzeige der �bersicht aller Spuren der CD. Die folgenden 
          Informationen werden angezeigt: 

             Tracks: 1 - 17
             Size: 55 min 34 sec 15 frames
             UPC/EAN: ??????????????

               Trk    start    length   track  Cpy Aud Pre
               N�   Mn Sc Fm  Mn Sc Fm  type   Pro Chn Emp
               001  00 02 00  14 36 55  data   yes  2  no
               002  14 38 55  03 36 16  audio  yes  2  no

          Zeile 1 zeigt die Anzahl der Spuren auf der CD an.  
          Zeile 2 gibt die Gesamtgr��e der CD an.  
          Zeile 3 stellt den UPC/EAN code dar.  Dieser Code soll  eine
          CD  Reihe  identifizieren,  nur  wenige  �ltere CDs besitzen
          einen solchen Code, neuere CDs, besonders  Audio-CDs,  haben
          einen solchen.  
          Zeilen  4  und 5 beinhalten den Kopf und ab Zeile 6 wird der
          Tabellenk�rper angezeigt.   Jeder  Spur  wird  hierbei  eine
          Zeile  zugeordnet  und  folgende  Information angezeigt (von
          links nach rechts): 
              1) Spurnummer, 
              2) Spurstart Minute, Sekunde, Frame, 
              3) Spurl�nge Minute, Sekunde, Frame, 
              4) Daten- oder Audiospur, 
              5) kopiergesch�tzt ja/nein, 
              6) Anzahl von Audiokan�len, und 
              7) Preemphasis ja/nein.  
          
          _A_c_h_t_u_n_g: _c_d_p hat keinen eingebauten Pager, so da� bei gro�en 
          CDs Zeilen aus dem Bildschirm geschoben werden.   Durch  die
          Eingabeumlenkung  in  das  Programm  mmoorree  kann  das behoben
          werden, zum Beispiel: "cdp tracks | more".  
          
      _p_)_l_a_y Play audio  tracks.      Audiospuren   abspielen.      Ein
          zus�tzliches  Argument teilt _c_d_p mit, welche Spuren gespielt
          werden sollen: 
             # nur Spur Nummer # spielen.  
             #- alle Spuren, beginnend mit Nummer #, spielen 
             #-# die Spuren von # bis # abspielen.  
          Ohne  dieses  Argument  wird  "1-"  angenommen,  was   einem
          Abspielen der   gesamten   CD  gleichkommt.    Zwischen  dem
          Bindestrich und der Nummer sind keine Leerzeichen erlaubt!  
          
          Falls  eine  Spurnummer  eine  Datenspur  oder  eine   nicht
          existente  Spur  angibt,  wird diese Spur �bersprungen, ohne
          da� eine  Fehlermeldung  ausgegeben  wird.     Nachdem   die
          Audiowidergabe  gestartet  wurde, wird mit der Meldung "CDP:
          Playing  tracks  #-#."  dar�ber  informiert,  welche  Spuren
          tats�chlich gespielt werden.  
          
      _i_)_s_p_l_a_y_i_n_g  Test,  ob  zur  Zeit  die  Audiowidergabe aktiv ist.
          Falls NICHT, wird  _c_d_p  mit  der  Fehlernummer  (Exit  code,
          errorlevel) 100 beendet.  Ansonsten wird ganz normal mit dem 
          n�chsten Kommando   fortgesetzt.      Dieses  Kommando  kann
          eingesetzt   werden,    um    herauszubekommen,    ob    die


                                      -3-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          Audiowidergabe gerade aktiv ist oder nicht.  
          
      _n_)_o_t_p_l_a_y_i_n_g Das  ist  das  Gegenst�ck  zu iissppllaayyiinngg.  Es beendet
          _c_d_p, FALLS die Audiowidergabe aktiv ist.  
          
      _d_o_o_r_o_)_p_e_n_e_d Test, ob der Schlitten offen ist.   Falls  JA,  wird
          mit  dem  n�chsten  Kommando  fortgesetzt, ansonsten mit der
          Fehlernummer (Exit code, errorlevel) 101 geendet.  
          
      _d_o_o_r_c_)_l_o_s_e_d Das ist das Gegenst�ck  zu  ddoooorrooppeenneedd.    _c_d_p  wird
          beendet, FALLS der Schlitten offen ist.  
          

     DDEEFFAAUULLTT AACCTTIIOONN: 

          
          Ist  kein Kommando auf der Kommandozeile angegeben, wird ein
          Standardkommando ausgef�hrt.  Dieses  h�ngt  vom  Namen  der
          gestarteten Datei ab.  Beginnt der Name mit: 
             EJECT 
             CLOSE 
          wird   das   gleichnamige   Kommando  ausgef�hrt,  ansonsten
          "PLAY".  
          

     KKNNOOWWNN BBUUGGSS: 

      _o Die Fehlerbehandlung ist rudiment�r.  Eine Meldung wie  "Error
          10"  deutet  darauf  hin, da� der CD-ROM Treiber eine Aktion
          nicht ausf�hren  konnte,  obwohl  sie  zu  diesem  Zeitpunkt
          ausf�hrbar sein  mu�.   Das kann verschiedene Ursachen habe,
          zum Beispiel: 
              a) der  CD-ROM  Treibername  oder  die  Unitnummer  sind
              falsch (vergleiche die Optionen /D: und /U:), 
              b) die CD-ROM wird gerade als Daten-CD genutzt, oder 
              c)  das  CD-ROM  Laufwerk  ist gerade mit der Ausf�hrung
              eines Kommandos besch�ftigt, ohne den Treiber  davon  in
              Kenntnis  zu  setzen, zum Beispiel w�hrend des manuellen
              �ffnens und Schlie�ens des Schlittens.  
          

     EEXXAAMMPPLLEESS: 

          
      _c_d_p 
          Startet die Audiowidergabe der gesamten CD. Alle Datenspuren 
          werden ignoriert, der Schlitten wird bei Bedarf  automatisch
          geschlossen.  
          
      _e_j_e_c_t_c_d 
          �ffnen des Schlittens. Das setzt voraus, da� die ausf�hrbare 
          Datei  in  "EJECTCD.COM"  umbenannt  oder kopiert wurde, zum
          Beispiel mit "REN CDP.COM EJECTCD.COM"  oder  "COPY  CDP.COM
          EJECTCD.COM".  
          
      _e_j_e_c_t_c_d_ _1_0_- 
          Spielt alle Audiospuren, beginnend mit der Spur 10, ab.  
          


                                      -4-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


      _c_d_p_ _/_d_:_o_p_t_ _/_u_:_3_ _e_ _/_d_ _4 
          �ffnet den Schlitten des Ger�ts Nummer 3 des Treibers "OPT", 
          danach  Starten  der  Audiowidergabe  der  Spur 4 des Ger�ts
          Nummer 3 des Standardtreibers.  
          
      _c_d_p_ _i_s_p_l_a_y_i_n_g 
          Die Batchdatei: 
              @echo off
              cdp i
              if not errorlevel 1 goto isPlaying
              if errorlevel 100 if not errorlevel 101 goto notPlaying
              echo Ein Fehler ist aufgetreten.
              goto end
              :notPlaying
              echo Audiowidergabe nicht aktiv.
              goto end
              :isPlaying
              echo Audiowidergabe aktiv.
              :end
          stellt fest, ab gerade die Audiowidergabe  aktiv  ist,  oder
          nicht, und gibt eine dementsprechende Meldung aus.  
          
      _c_d_p_ _i_s_p_l_a_y_i_n_g_ _s_t_o_p 
          Falls  die  Audiowidergabe  aktiv  ist, wird diese gestoppt.
          Ansonsten werden keine �nderungen vorgenommen.    W�rde  das
          Kommando  "stop"  allein angegeben, w�rde auf jeden Fall der
          Schlitten geschlossen  und  das  Treiber  neu  initialisiert
          werden.  
          

     EERRRROORRLLEEVVEELLSS: 

          
          Die  folgenden  Fehlernummern  (Exit Code, Errorlevel) haben
          eine spezielle Bedeutung: 
          
      _0 Kein Fehler.  
          
      _1_2_7 Hilfebildschirm angezeigt.  
          
      _1_0_0  Die  Kommandos  "iissppllaayyiinngg"  und  "nnoottppllaayyiinngg"  habe  einen
          gegens�tzlichen Zustand vorgefunden.  
          
      _1_0_1  Die  Kommandos  "ddoooorrooppeenneedd"  und  "ddoooorrcclloosseedd"  habe einen
          gegens�tzlichen Zustand vorgefunden.  
          
      _a_n_s_o_n_s_t_e_n Ein Fehler ist aufgetreten.  
          

     CCOONNTTRRIIBBUUTTEERRSS: 

                            Steffen Kaiser
                       Ernst-Th�lmann-Stra�e 2
                             D-39606 Iden
                        Deutschland - Germany
          
          e-mail: Steffen.Kaiser@Informatik.TU-Chemnitz.DE 
          


                                      -5-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          
          Gro�en  Dank  an  Dave  Dunfield,  welcher  diesen  herrlich
          kleinen Compiler "Micro-C" als Freeware verf�gbar machte.  

























































                                      -6-

