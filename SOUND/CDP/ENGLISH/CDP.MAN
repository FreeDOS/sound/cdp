
     CDP (12 Feb 1996)       Command Line CD Player                     CDP



     NNAAMMEE: 

          CDP - Command line Audio CD player 

     SSYYNNOOPPSSIISS: 

          _c_d_p [{ooppttiioonn | ccoommmmaanndd}] 
          

     DDEESSCCRRIIPPTTIIOONN: 

          _c_d_p is a command line driven audio CD player, which need not 
          MSCDEX loaded and does not stay resident (no TSR).  
          
          To  bypass  MSCDEX  saves  approximately  16KB  memory,  but
          requires a little bit more setup.   That  also  means,  that
          audio  tracks  can  be accessed now, but data tracks cannot.
          But  nonetheless,  if  MSCDEX  is  loaded,  _c_d_p  will   work
          properly.  
          
          _c_d_p does not stay resident, what means, that it will not use 
          any memory after execution.  This also means, that there are 
          no  such  functions like ppllaayy rraannddoommllyy or ppllaayy ccoonnttiinnuuoouussllyy,
          because both would  require  that  _c_d_p  stays  resident  and
          aquires memory and time.  
          
          All  options  and commands are processed from left to right.
          See the EXAMPLES section for more details.  
          
          If no command is specified  on  the  command  line,  one  is
          assumed.  See the DEFAULT ACTION section.  
          
          Before  processing any command line argument the contents of
          the environment variable "SSKKAAUUSS1188AA" is processed.   It  will
          be  interpreted the same way as if it would be a part of the
          command  line,  except  that  all  non-option  keywords  are
          ignored.   For instance, if the environment variable was set
          with "SET SKAUS18A= /D:NECD001 /U:2  play",  any  subsequent
          start  of  _c_d_p  acts  as if "/D:NECD001" and "/U:2" had been
          typed as the first two arguments.   The  command  "play"  is
          ignored.  Invalid options will cause an error.  
          

     OOPPTTIIOONNSS: 

          
      _/_?_ _a_n_d_ _/_h Display a help screen.  
          
      _/_D_:_n_a_m_e  Use  the  driver  nnaammee  for accessing the CD-ROM drive.
          This is the same name as of the option of  MSCDEX  with  the
          same  name, and, mostly, of the option of your CD-ROM driver
          with the same name.  Unless changed  by  _C_D_P__I_N_S_T  the  name
          defaults to: "MSCD001".  
          
      _/_D Reset the driver name to the default value.  
          
      _/_U_:_# Use  the unit number # of the CD-ROM drive.  Unless changed


                                      -1-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          by _C_D_P__I_N_S_T the unit number defaults to: "0".  
          
      _/_U Reset the value to the default value.  
          
      _/_[_+_-_]_A Auto-Unlock. If the '+' sign is supplied, Auto-Unlock  is
          active;  if  the '-' sign is supplied, Auto-Unlock disabled;
          otherwise the actual value is toggled.   Unless  changed  by
          _C_D_P__I_N_S_T the value defaults to: "disabled".  
          
          CD-ROM  drives with a tray allows to open and close the tray
          automaticly.  To avoid problems when the door is opened at a 
          wrong time, the door can be locked.    Auto-Unlock  controls
          whether  or  not  this  lock  is  automaticly  removed, if a
          command to open the door is to be executed.  
          
      _/_V Display version information, then exit.  
          

     CCOOMMMMAANNDDSS: 

          
          Each command may be abbreviated down to the string  left  of
          the paranthise  and will ignore the case of the letters.  Do
          not include the paranthise in the command!  
          
          If the command needs a closed door in order to be  processed
          but  the  door is currently opened, the "close" command will
          be executed prior the command itself.  
          
      _e_)_j_e_c_t Open the door.  If the door is locked and Auto-Unlock  is
          disabled, an  error  occures.  This command is not supported
          by all CD-ROM drives.  
          
      _c_)_l_o_s_e Close the door and initialize the  CD-ROM  drive.    This
          procedure includes to stop audio playing.  To close the tray 
          is not supported by all CD-ROM drives.  
          
      _s_)_t_a_t_u_s Display  the  status of the CD-ROM drive.  This contains
          more or less useful information.  
          
      _s_t_o_)_p This is equal to the cclloossee command.  
          
      _l_)_o_c_k Lock the door.  To lock the door is not supported  by  all
          CD-ROM drives.  
          
      _u_)_n_l_o_c_k Unlock  the  door.  To lock the door is not supported by
          all CD-ROM drives.  
          
      _t_)_r_a_c_k_s Display an overview of all tracks  of  the  CD-ROM.  The
          following information are displayed: 

             Tracks: 1 - 17
             Size: 55 min 34 sec 15 frames
             UPC/EAN: ??????????????

               Trk    start    length   track  Cpy Aud Pre
               N�   Mn Sc Fm  Mn Sc Fm  type   Pro Chn Emp
               001  00 02 00  14 36 55  data   yes  2  no


                                      -2-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


               002  14 38 55  03 36 16  audio  yes  2  no

          Line 1 tells how many tracks are available on the CD.  
          Line  2  tells  the  total size of the CD including any data
          tracks.  
          Line 3 shows the UPC/EAN code.  This code was introduced  to
          identify a specific CD series, in the past only few CDs have 
          such code, but most newly made CDs come with such code.  
          Lines  4  and 5 are the header and beginning with Line 6 the
          body of the track table.  Each track contains the  following
          information, from left to right: 
              1) track number, 
              2) start minute, second, frame, 
              3) length of the track minute, second, frame, 
              4) data or audio track, 
              5) copy protected yes/no, 
              6) number of audio channels, and 
              7) pre-emphasis yes/no.  
          
          _N_o_t_e: There  is  no  built-in pager for the output.  To page
          the output just pipe the command  into  mmoorree,  for  example:
          "cdp tracks | more".  
          
      _p_)_l_a_y Play  audio  tracks.   An additional argument may tell _c_d_p
          which tracks shall be played.  This argument looks like: 
             # To play only track # 
             #- To play all tracks beginning at # 
             #-# To play the tracks from # to # 
          Without this argument "1-" is assumed, what means  that  the
          whole CD  will be played.  No whitespaces are allowed around
          the dash!  
          
          If one of the track numbers identifies a  data  track  or  a
          non-existing track, this track is skipped, but no warning is 
          issued.   After  successfully  initiating the audio play the
          message  "CDP:  Playing  tracks  #-#."  will  be   displayed
          informing about which tracks are really played.  
          
      _i_)_s_p_l_a_y_i_n_g Test,  if audio is playing.  If audio is NOT playing,
          _c_d_p will be terminated with the errorlevel  100.  Otherwise,
          the next  command  will  be  executed.   This can be used to
          determine, whether  or  not  an  audio  track  is  currently
          playing.  See section EXAMPLES.  
          
      _n_)_o_t_p_l_a_y_i_n_g This   is  the  opposite  of  iissppllaayyiinngg.    It  will
          terminate with the errorlevel 100, if audio IS playing.  
          
      _d_o_o_r_o_)_p_e_n_e_d Test, if the door is currently opened.    If  it  IS
          opened,  the  next  command  will be executed, otherwise _c_d_p
          will terminate with the exit code (errorlevel) 101.  
          
      _d_o_o_r_c_)_l_o_s_e_d This  is  the  opposite  of  ddoooorrooppeenneedd.    It  will
          terminate  with  the  errorlevel  101,  if  the  door is NOT
          opened.  
          

     DDEEFFAAUULLTT AACCTTIIOONN: 



                                      -3-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          
          When no command is specified  on  command  line,  a  default
          action will be initiated.  This action depends on the actual 
          name of the executable.  If the executable's file name start 
          with  one  of the following names, the equally named command
          will be activated: 
             EJECT 
             CLOSE 
          Otherwise the default action is "PLAY".  
          

     KKNNOOWWNN BBUUGGSS: 

      _o The error handling is not  great.    When  some  message  like
          "Error 10" is displayed, the CD-ROM driver did not activated 
          a function  as  it  should  at  this  time.    This can have
          multiple reasons, for example: 
              a) the CD-ROM settings are invalid (see options /D:  and
              /U:), 
              b) the CD-ROM is currently accessed as data CD, 
              c)  the drive was busy while initiating the command, but
              did not  notifed  the  driver  about  it,  e.g.    while
              manually opening/closing the door.  
          

     EEXXAAMMPPLLEESS: 

          
      _c_d_p 
          Start  playing  the whole CD ignoring all CD-ROM data tracks
          and close the tray, if it is currently opened.  
          
      _e_j_e_c_t_c_d 
          Eject the disk.  This requires that the executable has  been
          renamed  or  copied to "ejectcd.com", for instance by typing
          "REN CDP.COM EJECTCD.COM" or "COPY CDP.COM EJECTCD.COM".  
          
      _e_j_e_c_t_c_d_ _1_0_- 
          Play all audio tracks beginning with track "10".  
          
      _c_d_p_ _/_d_:_o_p_t_ _/_u_:_3_ _e_ _/_d_ _4 
          Eject the disk of the driver "OPT" subunit 3, then play  the
          audio track "4" of the default driver subunit 3.  
          
      _c_d_p_ _i_s_p_l_a_y_i_n_g 
          The batch script: 
              @echo off
              cdp i
              if not errorlevel 1 goto isPlaying
              if errorlevel 100 if not errorlevel 101 goto notPlaying
              echo An error occured.
              goto end
              :notPlaying
              echo Audio is not playing.
              goto end
              :isPlaying
              echo Audio is playing.
              :end


                                      -4-


     CDP (12 Feb 1996)       Command Line CD Player                     CDP


          determines   the  state  of  audio  playing  and  echos  the
          appropriate message.  
          
      _c_d_p_ _i_s_p_l_a_y_i_n_g_ _s_t_o_p 
          If  the  audio  is  playing,  the  play  will  be   stopped.
          Otherwise, no  changes  are  made  to the CD-ROM drive.  The
          command "stop" alone would close the tray and initialize the 
          CD-ROM driver.  
          

     EERRRROORRLLEEVVEELLSS: 

          
          The following errorlevels have a specific meaning: 
          
      _0 No error occured.  
          
      _1_2_7 Help screen issued.  
          
      _1_0_0 The commands "iissppllaayyiinngg" or "nnoottppllaayyiinngg" found the  opposite
          situation.  
          
      _1_0_1 The commands "ddoooorrooppeenneedd" or "ddoooorrcclloosseedd" found the opposite 
          situation.  
          
      _o_t_h_e_r_w_i_s_e An error occured.  
          

     CCOONNTTRRIIBBUUTTEERRSS: 

                            Steffen Kaiser
                       Ernst-Th�lmann-Stra�e 2
                             D-39606 Iden
                        Deutschland - Germany
          
          e-mail: Steffen.Kaiser@Informatik.TU-Chemnitz.DE 
          



          
          Many  thanks  to  Dave  Dunfield, who released this fabulous
          little compiler "Mirco-C" as Freeware.  

















                                      -5-

